'use strict';

const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const xml = require('xml');
const PNG = require('pngjs').PNG;
const argv = require('minimist')(process.argv.slice(2));

const src = fs.createReadStream(path.resolve(argv._.shift()));
const image = new PNG();

function rgbToHex(r, g, b) {
  let hex = ((r << 16) + (g << 8) + b).toString(16);
  while (hex.length < 6) hex = '0' + hex;
  return '#' + hex;
}

function convert(image) {
  const data = [];

  data.height = image.height;
  data.width = image.width;

	for (let y = 0; y < image.height; y++) {
		for (let x = 0; x < image.width; x++) {
			let i = (image.width * y + x) << 2;

      const alpha = image.data[i + 3] / 255;
      if (alpha === 0) continue;

      const red = image.data[i];
      const green = image.data[i + 1];
      const blue = image.data[i + 2];

      const color = rgbToHex(red, green, blue);

      data.push({
        fill: color,
        x: x,
        y: y,
        opacity: alpha,
      });

		}
	}

  return data;

}

function onePixel(image) {
  for (let i = 0; i < image.length; i++)
    image[i].d = 'M' + image[i].x + ' ' + image[i].y + 'h1v1h-1v-1z';

  return image;
}

function next(pixels, query, position) {

  const next = _.find(pixels, query);
}

function polygon(image) {
  const defpixel = {
    d: 'M',
    from: 0,
    coord: [],
  };

  const data = [];
  let next;
  let pixel = _.assign({}, defpixel);

  const to = [
    {
      d: 'v-1',
      from: 3,
      x: 0,
      y: -1,
    },
    {
      d: 'h1',
      from: 0,
      x: 1,
      y: 0,
    },
    {
      d: 'v1',
      from: 1,
      x: 0,
      y: 1,
    },
    {
      d: 'h-1',
      from: 2,
      x: -1,
      y: 0,
    },
  ];

  while (image.length > 0) {
    //console.error(pixel.coord);
    let times = 0;
    let next;

    if (pixel.d === 'M') {
      next = image[0];
      pixel.fill = next.fill;
    }
    else {
      console.error('from', pixel.from)
      while (times < 4) {
        let query = {
          fill: pixel.fill,
          x: pixel.x + to[(pixel.from + times) % 4].x,
          y: pixel.y + to[(pixel.from + times) % 4].y,
        };
        next = _.find(image, query);
        //console.error(to[(pixel.from + times) % 4])
        console.error('q', query, 'next', next)
        console.error('coord', pixel.coord)

        if (next && next.x === pixel.coord[0].x && next.y === pixel.coord[0].y) {
          next = undefined;
          break;
        }
        else if (next && (next.x !== pixel.coord[0].x || next.y !== pixel.coord[0].y)) {
          break;
        }

        times++;
      }
    }

    if (next) {
      pixel.x = next.x;
      pixel.y = next.y;
      pixel.from = to[(pixel.from + times) % 4].from;
      pixel.d += to[(pixel.from + times) % 4].d;

      pixel.coord.push({x: pixel.x, y: pixel.y});
    }
    else {
      pixel.d += 'z';
      data.push(pixel);

      pixel.coord.push({x: pixel.x, y: pixel.y });
      pixel.coord.forEach(coord => _.remove(image, coord));
    }

  }

  return data;
}

function toXML(image) {
  const svg = [{
    _attr: {
      xmlns: 'http://www.w3.org/2000/svg',
      width: image.width,
      height: image.height,
      viewBox: '0 0 ' + image.width + ' ' + image.height,
    },
  }];

  for (let i = 0; i < image.length; i++)
    svg.push({
      path: [{
        _attr: {
          fill: image[i].fill,
          d: image[i].d,
          opacity: image[i].opacity,
        },
      }],
    });

  return xml({svg: svg});
}

image.on('parsed', buffer => {
  let data = convert(image);
  data = polygon(data);
  data = toXML(data);

  console.log(data);
});

src.pipe(image);
