# Vecart

## What?
Vector pixelart parser.

Basically, this would convert your PNG pixelart to SVG with 1:1 conversion.

## Why?
I love viewing pixelart diffs. Since every pixel is so important.

## Usage
```sh
vecart image.png
```

This will output source svg to stdout.
You could combine it with [svgo](npmjs.com/package/svgo).

## License
MIT
